# Mi Aplicación

Descripción breve de la aplicación.

## Requisitos

- PHP 7.4 o superior
- Composer

## Instalación

1. Clona este repositorio: `git clone https://gitlab.com/juanhetcas04/api-symfony`
2. Instala las dependencias: `composer install`
3. Crea una base de datos y configura las variables de entorno en `.env`

## Uso

1. Ejecuta el servidor local de Symfony: `symfony server:start`
2. Accede a la aplicación en tu navegador: `http://localhost:8000`

## Pruebas

Ejecuta las pruebas unitarias con PHPUnit:

```bash
php bin/phpunit
