<?php

$parameters = [
    'database_host' => $_ENV['DB_HOST'],
    'database_port' => $_ENV['DB_PORT'],
    'database_name' => $_ENV['DB_NAME'],
    'database_user' => $_ENV['DB_USER'],
    'database_password' => $_ENV['DB_PASSWORD'],
];

return $parameters;
