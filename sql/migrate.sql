-- Crear la base de datos
CREATE DATABASE my_database;

-- Usar la base de datos
USE my_database;

-- Tabla de usuarios
CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    roles JSON NOT NULL
);

-- Tabla de categorías
CREATE TABLE categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

-- Tabla de productos
CREATE TABLE products (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE SET NULL
);

-- Insertar algunas categorías
INSERT INTO categories (name) VALUES ('Electrónica');
INSERT INTO categories (name) VALUES ('Ropa');
INSERT INTO categories (name) VALUES ('Hogar');

-- Insertar un usuario para pruebas
INSERT INTO users (username, password, roles)
VALUES ('testuser', '$2y$12$Jkc/lB5UehSYg.hv9kLzUOGe1blJtbmIny28XHXbpxY4cnJiCuj/2', '["ROLE_USER"]');
