<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\ProductService;

class ProductController extends AbstractController
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function createProduct(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        // Crear el producto y manejar la lógica
        $product = $this->productService->createProduct($data);

        return $this->json(['message' => 'Producto creado con éxito', 'product' => $product]);
    }

    // Otros métodos relacionados con productos
}
