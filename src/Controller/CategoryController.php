<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\CategoryService;

class CategoryController extends AbstractController
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function createCategory(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        // Crear la categoría y manejar la lógica
        $category = $this->categoryService->createCategory($data);

        return $this->json(['message' => 'Categoría creada con éxito', 'category' => $category]);
    }

    // Otros métodos relacionados con categorías
}
