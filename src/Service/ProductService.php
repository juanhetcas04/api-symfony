<?php

namespace App\Service;

use App\Model\ProductRepository;
use App\Model\CategoryRepository;

class ProductService
{
    private $productRepository;
    private $categoryRepository;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function createProduct(array $data): array
    {
        // Validar datos y crear el producto
        $category = $this->categoryRepository->find($data['category_id']);
        $product = $this->productRepository->create($data, $category);

        // Realizar otras operaciones

        return $product;
    }

    // Otros métodos relacionados con productos
}
