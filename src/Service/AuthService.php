<?php

namespace App\Service;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Firebase\JWT\JWT;
use App\Model\UserRepository;

class AuthService
{
    private $userRepository;
    private $passwordEncoder;
    private $jwtSecret;

    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder, $jwtSecret)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->jwtSecret = $jwtSecret;
    }

    public function login(string $username, string $password): string
    {
        // Validar credenciales y obtener el usuario

        $user = $this->userRepository->findByUsername($username);

        if (!$user || !$this->passwordEncoder->isPasswordValid($user, $password)) {
            throw new \Exception('Credenciales inválidas');
        }

        // Generar y retornar token JWT
        $payload = [
            'user_id' => $user->getId(),
            // Otras reclamaciones
        ];

        return JWT::encode($payload, $this->jwtSecret);
    }

    // Otros métodos relacionados con la autenticación
}
