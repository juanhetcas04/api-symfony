<?php

namespace App\Service;

use App\Model\CategoryRepository;

class CategoryService
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function createCategory(array $data): array
    {
        // Validar datos y crear la categoría
        $category = $this->categoryRepository->create($data);

        // Realizar otras operaciones

        return $category;
    }

    // Otros métodos relacionados con categorías
}
