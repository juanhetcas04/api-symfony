<?php

namespace App\Model;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Category;

class CategoryRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(array $data): Category
    {
        $category = new Category();
        $category->setName($data['name']);

        // Realizar más configuraciones

        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $category;
    }

    // Otros métodos relacionados con categorías
}
