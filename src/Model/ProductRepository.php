<?php

namespace App\Model;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;
use App\Entity\Category;

class ProductRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(array $data, Category $category): Product
    {
        $product = new Product();
        $product->setName($data['name']);
        $product->setCategory($category);

        // Realizar más configuraciones

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

    // Otros métodos relacionados con productos
}
