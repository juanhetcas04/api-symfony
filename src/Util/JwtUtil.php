<?php

namespace App\Util;

use Firebase\JWT\JWT;

class JwtUtil
{
    private $jwtSecret;

    public function __construct($jwtSecret)
    {
        $this->jwtSecret = $jwtSecret;
    }

    public function decodeToken(string $token): object
    {
        return JWT::decode($token, $this->jwtSecret, ['HS256']);
    }

    // Otros métodos relacionados con JWT
}
